//! A skeleton for a new module

use serde::Deserialize;

use crate::geometry::{Area, Point, Rectangle};
use crate::root_window::DisplayOnRoot;

/// The [`SkelMod`] struct.
#[derive(Default, Deserialize, Debug)]
pub struct SkelMod {
    // put the config file parameters here
}

impl SkelMod {
    /// Returns a virtual Rectangle containing the module data.
    ///
    /// This is used for fitting/overlapping calculations.
    pub fn shape(&self) -> Rectangle {
        // implement this
    }
}

/// This trait is used to do the actual drawing on the root screen.
impl DisplayOnRoot for SkelMod {
    fn display_content(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        screen: &xcb::x::Screen,
    ) {
        // The implementation of this function usually involves the creation of a
        // graphical context, see a minimal setting below:
        conn.send_request(&xcb::x::CreateGc {
            cid: gc,
            drawable,
            value_list: &[
                xcb::x::Gc::Foreground(screen.white_pixel()),
                xcb::x::Gc::Background(screen.black_pixel()),
            ],
        });
        // followed by a drawing request (below an example of the drawing of a
        // rectangle):
        conn.send_request(&xcb::x::PolyRectangle {
            drawable,
            gc,
            rectangles: &vec![self.shape().as_x_rectangle()],
        });
    }
}
