//! This is a test module displaying "hello world" on screen
//!
use anyhow::anyhow;
use fontconfig::Fontconfig;
use freetype::face::LoadFlag;
use freetype::{Face, Library};
use serde::Deserialize;

use crate::geometry::{Area, Point, Rectangle};
use crate::root_window::DisplayOnRoot;
use crate::utils::gen_points_from_glyph;

/// The [`HelloWorldMod`] struct.
#[derive(Default, Deserialize, Debug)]
pub struct HelloWorldFreetypeMod {
    x: i16,
    y: i16,
    font: String,
    fontstyle: Option<String>,
    string: Option<String>,
    draw_rectangle: Option<bool>,
}

impl HelloWorldFreetypeMod {
    const MSG: &'static str = "Hello, World!";

    pub fn msg(&self) -> String {
        match &self.string {
            Some(value) => value.clone(),
            None => Self::MSG.to_owned(),
        }
    }

    pub fn shape(&self) -> Rectangle {
        let mut width = 0;
        let mut height = 0;
        // let mut top = 0;
        let face = self.create_face().expect("Unable to create face");
        for c in self.msg().chars() {
            face.load_char(c as _, LoadFlag::RENDER | LoadFlag::MONOCHROME)
                .expect("Unable to load char in face");
            let glyph = face.glyph();
            // println!("h adv: {}", glyph.linear_hori_advance() as f64 / 65536.0);
            // println!("v adv: {}", glyph.linear_vert_advance() as f64 / 65536.0);
            width += (glyph.linear_hori_advance() as f64 / 65536.0).round() as i16;
            height = std::cmp::max(
                (glyph.linear_vert_advance() as f64 / 65536.0).round() as i16,
                height,
            );
            //top = std::cmp::max(top, glyph.bitmap_top());
        }

        // println!("DBG: top = {top}");
        println!("DBG: found shape for HelloWorldFreetypeMod: height = {height}, width = {width}"); //, x = {}, y = {}", self.x, self.y as i32 - top);
        Rectangle::new(Point::new(self.x, self.y), Area::new(height, width))
    }
}

impl DisplayOnRoot for HelloWorldFreetypeMod {
    fn display_content(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        screen: &xcb::x::Screen,
    ) {
        conn.send_request(&xcb::x::CreateGc {
            cid: gc,
            drawable,
            value_list: &[
                xcb::x::Gc::Foreground(screen.white_pixel()),
                xcb::x::Gc::Background(screen.black_pixel()),
            ],
        });
        if self.draw_rectangle.unwrap_or(false) {
            conn.send_request(&xcb::x::PolyRectangle {
                drawable,
                gc,
                rectangles: &vec![self.shape().as_x_rectangle()],
            });
        }
        conn.send_request(&xcb::x::PolyPoint {
            coordinate_mode: xcb::x::CoordMode::Origin,
            drawable,
            gc,
            // TODO: properly handle error
            points: &self
                .create_points()
                .expect("Unable to create points from text"),
        });
    }
}

impl HelloWorldFreetypeMod {
    fn create_face(&self) -> anyhow::Result<Face> {
        let fc = Fontconfig::new().ok_or_else(|| anyhow!("Unable to initialize fontconfig"))?;
        let font = fc
            .find(&self.font, self.fontstyle.as_deref())
            .ok_or_else(|| anyhow!("Unable to find font: {}", self.font))?;

        let ft = Library::init().expect("Unable to initialize freetype lib");
        let face = ft
            .new_face(&font.path, 0)
            .unwrap_or_else(|_| panic!("Unable to load font at {}", font.path.display()));

        face.set_char_size(40 * 64, 0, 50, 0).unwrap();
        Ok(face)
    }

    fn create_points(&self) -> anyhow::Result<Vec<xcb::x::Point>> {
        let face = self.create_face()?;

        // Compute max bitmap_top for y_offset calculation
        let mut top = 0;
        for c in self.msg().chars() {
            face.load_char(c as _, LoadFlag::RENDER | LoadFlag::MONOCHROME)?;
            let glyph = face.glyph();
            top = std::cmp::max(top, glyph.bitmap_top());
        }
        let y_offset = top as i16 + self.y + 2; // Add 2 for better top border
        let mut total_offset = self.x; // initial absolute offset

        // Compute points
        let mut points = Vec::new();
        for c in self.msg().chars() {
            face.load_char(c as _, LoadFlag::RENDER | LoadFlag::MONOCHROME)?;
            let glyph = face.glyph();
            let advance = glyph.advance();
            points.append(&mut gen_points_from_glyph(glyph, total_offset, y_offset));
            total_offset += (advance.x / 64) as i16;
        }

        Ok(points)
    }
}
