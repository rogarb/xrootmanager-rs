//! A module to display a list (TODO, movie list, shopping list...). It can list
//! a directory or list the lines of a text file.
//!
//! This is based on the 90's (deprecated) way of drawing text with Xorg, which
//! is IMHO simpler than the current fontconfig/freetype/bitmap rendering stack.
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

use serde::Deserialize;

use crate::geometry::{Area, Point, Rectangle};
use crate::root_window::{DisplayOnRoot, RootWindowAttributes};
use crate::utils::{compute_extents, create_char2b_vec};

/// The [`ListMod`] struct.
#[derive(Default, Deserialize, Debug)]
pub struct ListMod {
    origin: Point,
    area: Option<Area>,
    content: PathBuf,
    lines: Option<usize>,
    font: Option<String>,
    list: Option<ListEnum>,
    title: Option<String>,
    color: Option<Color>,
    background_color: Option<Color>,
    ignore_comments: Option<bool>,
}

#[derive(Deserialize, Debug, Copy, Clone)]
enum ListEnum {
    #[serde(rename = "enum")]
    Enum,
    #[serde(rename = "bullet")]
    Bullet,
    #[serde(rename = "dash")]
    Dash,
}

impl ListEnum {
    fn chr(&self) -> Option<char> {
        match self {
            Self::Enum => None,
            Self::Bullet => Some('*'),
            Self::Dash => Some('-'),
        }
    }
}

// Colors are defined as their RGB value in hex format
#[derive(Deserialize, Debug, Copy, Clone)]
enum Color {
    #[serde(rename = "black")]
    Black = 0xFFFFFF,
    #[serde(rename = "white")]
    White = 0x000000,
    #[serde(rename = "red")]
    Red = 0xFF0000,
    #[serde(rename = "green")]
    Green = 0x00FF00,
    #[serde(rename = "blue")]
    Blue = 0x0000FF,
}

impl Default for Color {
    fn default() -> Self {
        Self::Black
    }
}

impl Color {
    pub fn complementary(&self) -> u32 {
        *self as u32 ^ Self::Black as u32
    }
}

impl ListMod {
    const DEFAULT_FONT: &'static str = "fixed";

    /// Returns a virtual Rectangle containing the module data.
    ///
    /// This is used for fitting/overlapping calculations.
    pub fn shape(&self) -> Rectangle {
        // if an area is defined, there is no need to calculate the space occupied
        // by the text as it will be fitted to the area.
        if let Some(area) = self.area {
            Rectangle::new(self.origin, area)
        } else {
            let mut width = 0;
            let mut height = 0;
            for line in self
                .get_content()
                .expect("Unable to get file content!")
                .lines()
            {
                let extents = compute_extents(self.font().as_str(), line).unwrap();
                width = std::cmp::max(extents.width, width);
                height += extents.height;
            }
            Rectangle::new(self.origin, Area::new(height, width))
        }
    }

    /// Returns the content of the file as a String
    fn get_content(&self) -> Result<String, std::io::Error> {
        let mut content = String::new();
        if self.content.is_dir() {
            content = std::fs::read_dir(self.content.as_path())?
                // TODO: handle Results
                .map(|entry| format!("{}\n", entry.unwrap().file_name().into_string().unwrap()))
                .collect::<String>();
        } else {
            let mut file = File::open(self.content.as_path())?;
            file.read_to_string(&mut content)?;
        }
        // keep only required lines
        if let Some(n) = self.lines {
            content = content
                .lines()
                .take(n)
                .map(|s| {
                    let mut new = s.to_string();
                    new.push('\n');
                    new
                })
                .collect::<String>();
        }
        // println!("DBG: content: {content:?}");
        Ok(content)
    }

    /// Create a graphical context for
    fn create_gc(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        _screen: &xcb::x::Screen,
    ) {
        // let fontname = String::from("lucidasanstypewriter-bold-24");
        let font: xcb::x::Font = conn.generate_id();
        conn.send_request(&xcb::x::OpenFont {
            fid: font,
            name: self.font().as_bytes(),
        });
        conn.send_request(&xcb::x::CreateGc {
            cid: gc,
            drawable,
            value_list: &[
                xcb::x::Gc::Foreground(self.color.unwrap_or_default() as u32),
                xcb::x::Gc::Background(
                    self.background_color
                        .map(|c| c as u32)
                        .unwrap_or_else(|| self.color.unwrap_or_default().complementary()),
                ),
                xcb::x::Gc::Font(font),
            ],
        });
    }

    /// Returns the font to be used
    fn font(&self) -> String {
        self.font
            .as_deref()
            .unwrap_or(Self::DEFAULT_FONT)
            .to_owned()
    }
}

/// This trait is used to do the actual drawing on the root screen.
impl DisplayOnRoot for ListMod {
    fn display_content(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        screen: &xcb::x::Screen,
    ) {
        self.create_gc(gc, drawable, conn, screen);

        // TODO: move all calculations to an init() function, as they don't need
        // to be done everytime.

        // create the extent struct for the case of displaying an enum
        let lines = self
            .get_content()
            .expect("Unable to get text content")
            .lines()
            .count();
        // Redefine compute_extents as we have access to a fully set up X connexion
        let compute_extents = |string: &str| {
            let cookie = conn.send_request(&xcb::x::QueryTextExtents {
                font: xcb::x::Fontable::Gcontext(gc),
                string: &create_char2b_vec(string),
            });
            conn.wait_for_reply(cookie)
                .expect("Unable to compute extents for {string:?}")
        };

        // let enum_extent = compute_extents(&format!(" {lines}. "));

        let mut y_offset = 5;
        if let Some(title) = &self.title {
            let title_extent = compute_extents(title);
            y_offset += title_extent.overall_ascent() + title_extent.overall_descent();
            conn.send_request(&xcb::x::ImageText16 {
                drawable,
                gc,
                x: self.origin.x + self.shape().dimensions.width / 2, // center title
                y: self.origin.y + y_offset,
                string: &create_char2b_vec(title),
            });
            y_offset += title_extent.overall_ascent() + title_extent.overall_descent();
        }

        for (n, line) in self
            .get_content()
            .expect("Unable to get text content")
            .lines()
            .filter(|l| {
                if self.ignore_comments.unwrap_or_default() {
                    l.find('#') != Some(0)
                } else {
                    true
                }
            })
            .enumerate()
        {
            // Compute prefix
            let prefix = self
                .list
                .map(|prefix| {
                    format!(
                        " {} ",
                        prefix
                            .chr()
                            .map(|c| c.to_string())
                            .unwrap_or(format!("{}.", n + 1))
                    )
                })
                .unwrap_or_else(String::new);

            // The extent of the current prefix
            let prefix_extent = compute_extents(&prefix);

            // The extent of the widest prefix, different from prefix_extent only
            // when self.list == Some(ListEnum::Enum)
            // let widest_prefix_extent = match self.list {
            //     Some(ListEnum::Enum) => enum_extent,
            //     _ => prefix_extent,
            // };

            let extents = compute_extents(line);
            let mut line = vec![line.to_owned()];
            // if an area is defined, wrap long lines
            // println!("DBG: area: {:?}", self.area);
            if self
                .area
                .map(|a| extents.overall_width() + prefix_extent.overall_width() > a.width as i32)
                .unwrap_or(false)
            {
                let full_line = line.pop().unwrap(); // can't panic as the vec is not empty
                let mut tmp_line = String::new();
                for word in full_line.split(' ') {
                    if !tmp_line.is_empty() {
                        tmp_line.push(' ');
                    }
                    tmp_line.push_str(word);
                    if compute_extents(&tmp_line).overall_width() + prefix_extent.overall_width()
                        > self.area.unwrap().width as i32
                    {
                        tmp_line.truncate(tmp_line.len() - word.len());
                        line.push(tmp_line.clone());
                        tmp_line.clear();
                        tmp_line.push_str(word);
                    }
                }
                line.push(tmp_line);
                // do something to wrap
            }
            // Check that we don't go out of bonds
            if self.origin.y + y_offset
                > self.area.map(|a| a.width).unwrap_or_else(|| {
                    RootWindowAttributes::new()
                        .expect("Unable to get root window attributes")
                        .width
                })
            {
                break;
            }
            // Draw prefix
            y_offset += extents.overall_ascent();
            println!("DBG: y_offset = {y_offset}");
            conn.send_request(&xcb::x::ImageText16 {
                drawable,
                gc,
                x: self.origin.x,
                y: self.origin.y + y_offset,
                string: &create_char2b_vec(&prefix),
            });
            // draw line/sublines
            for (i, subline) in line.iter().enumerate() {
                conn.send_request(&xcb::x::ImageText16 {
                    drawable,
                    gc,
                    x: self.origin.x + prefix_extent.overall_width() as i16,
                    y: self.origin.y + y_offset,
                    string: &create_char2b_vec(subline),
                });
                y_offset += (extents.overall_ascent() + extents.overall_descent()) as i16;
                if i == 0 {
                    y_offset += extents.overall_descent();
                }
            }
            // if line.len() == 1 {
            //     y_offset -= extents.overall_ascent() as i16; // Remove extra height
            // }
        }
    }
}
