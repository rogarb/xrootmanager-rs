//! This module allows to draw a [`Circle`] on the root window. This was written
//! for testing purposes.
use crate::geometry::circle::Circle;
use crate::root_window::DisplayOnRoot;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct DrawCircleMod {
    pub circle: Circle,
}

impl DisplayOnRoot for DrawCircleMod {
    fn display_content(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        screen: &xcb::x::Screen,
    ) {
        conn.send_request(&xcb::x::CreateGc {
            cid: gc,
            drawable,
            value_list: &[
                xcb::x::Gc::Foreground(screen.white_pixel()),
                xcb::x::Gc::Background(screen.black_pixel()),
            ],
        });
        conn.send_request(&xcb::x::PolyPoint {
            coordinate_mode: xcb::x::CoordMode::Origin,
            drawable,
            gc,
            points: &self
                .circle
                .into_iter()
                .map(|p| p.as_x_point())
                .collect::<Vec<xcb::x::Point>>(),
        });
    }

    // fn create_gc(
    //     &self,
    //     gc: xcb::x::Gcontext,
    //     drawable: xcb::x::Drawable,
    //     conn: &xcb::Connection,
    // ) {
    //     conn.send_request(&xcb::x::CreateGc {
    //         cid: gc,
    //         drawable,
    //         value_list: &[
    //             xcb::x::Gc::Foreground(screen.white_pixel()),
    //             xcb::x::Gc::Background(screen.black_pixel()),
    //         ],
    //     });
    // }
}
