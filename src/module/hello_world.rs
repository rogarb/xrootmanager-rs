//! This is a test module displaying "hello world" on screen
//!
use serde::Deserialize;

use crate::geometry::{Area, Point, Rectangle};
use crate::root_window::DisplayOnRoot;
use crate::utils::create_char2b_vec;

/// The [`HelloWorldMod`] struct.
#[derive(Default, Deserialize, Debug)]
pub struct HelloWorldMod {
    x: i16,
    y: i16,
}

impl HelloWorldMod {
    const MSG: &'static str = "Hello, World!";

    pub fn shape(&self) -> Rectangle {
        let (conn, screen_num) = xcb::Connection::connect(None).unwrap();
        let setup = conn.get_setup();
        let screen = setup.roots().nth(screen_num as usize).unwrap();
        let window = screen.root();

        // Create custom GC
        let drawable = xcb::x::Drawable::Window(window);
        let gc: xcb::x::Gcontext = conn.generate_id();
        self.create_gc(gc, drawable, &conn, screen);

        let cookie = conn.send_request(&xcb::x::QueryTextExtents {
            font: xcb::x::Fontable::Gcontext(gc),
            string: &create_char2b_vec(Self::MSG),
        });
        let reply = conn.wait_for_reply(cookie).unwrap();
        let width = reply.overall_width() as i16;
        let height = reply.overall_ascent() + reply.overall_descent();
        println!("DBG: found shape for HelloWorldMod: height = {height}, width = {width}");
        Rectangle::new(Point::new(self.x, self.y), Area::new(height, width))
    }
}

impl DisplayOnRoot for HelloWorldMod {
    fn display_content(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        screen: &xcb::x::Screen,
    ) {
        self.create_gc(gc, drawable, conn, screen);
        conn.send_request(&xcb::x::ImageText8 {
            drawable,
            gc,
            x: self.x,
            y: self.y + self.shape().dimensions.height as i16,
            string: Self::MSG.as_bytes(),
        });
    }
}

impl HelloWorldMod {
    fn create_gc(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        screen: &xcb::x::Screen,
    ) {
        let fontname = String::from("lucidasanstypewriter-bold-24");
        let font: xcb::x::Font = conn.generate_id();
        conn.send_request(&xcb::x::OpenFont {
            fid: font,
            name: fontname.as_bytes(),
        });
        conn.send_request(&xcb::x::CreateGc {
            cid: gc,
            drawable,
            value_list: &[
                xcb::x::Gc::Foreground(screen.black_pixel()),
                xcb::x::Gc::Background(screen.white_pixel()),
                xcb::x::Gc::Font(font),
            ],
        });
    }
}

impl HelloWorldMod {
    pub fn new(x: i16, y: i16) -> Self {
        Self { x, y }
    }
}
