//! This module defines the [`Layout`] object, which wraps the different elements
//! to display on the root window.
use crate::config::Config;
// use crate::module::Module;
use crate::root_window::{DisplayOnRoot, RootWindowAttributes};

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

// use serde::Deserialize;
use anyhow::bail;

/// The [`Layout`] struct.
pub struct Layout {
    root_window: RootWindowAttributes,
    config: Config,
}

impl Layout {
    /// Display the [`Layout`] on screen
    pub fn display(self) -> xcb::Result<()> {
        // let mut threads = Vec::new();
        // for module in self.config.modules {
        //     threads.push(thread::spawn(move || module.display()));
        // }
        // for thread in threads {
        //     thread.join().map_err(|_| String::from("Module paniced"))?;
        // }
        let (conn, screen_num) = xcb::Connection::connect(None).unwrap();
        let setup = conn.get_setup();
        let screen = setup.roots().nth(screen_num as usize).unwrap();
        let window = screen.root();
        conn.send_request(&xcb::x::ChangeWindowAttributes {
            window,
            value_list: &[
                xcb::x::Cw::BackPixel(screen.black_pixel()),
                xcb::x::Cw::EventMask(xcb::x::EventMask::EXPOSURE),
            ],
        });

        conn.flush()?;
        loop {
            let event = match conn.wait_for_event() {
                Err(xcb::Error::Connection(xcb::ConnError::Connection)) => {
                    // graceful shutdown, likely "x" close button clicked in title bar
                    break;
                }
                Err(err) => {
                    panic!("unexpected error: {:#?}", err);
                }
                Ok(event) => event,
            };
            if let xcb::Event::X(xcb::x::Event::Expose(_ev)) = event {
                for module in &self.config.modules {
                    // Create a GC per module
                    let drawable = xcb::x::Drawable::Window(window);
                    let gc: xcb::x::Gcontext = conn.generate_id();
                    module.display_content(gc, drawable, &conn, screen);
                }
                /* We flush the request */
                conn.flush()?;
            }
        }
        Ok(())
    }

    /// Create a new [`Layout`] from a config file.
    pub fn new(config_file: &Path) -> anyhow::Result<Self> {
        // Parse config file
        let mut content = String::new();
        File::open(config_file)?.read_to_string(&mut content)?;
        let config: Config = toml::from_str(&content)?;
        println!("DBG: Config parsed");
        // Check Layout configuration
        let root_window = RootWindowAttributes::new()?;
        println!("DBG: Got root window attributes");
        println!(
            "DBG: Shapes:\n\troot window: {:?}\n\tlayout: {:?}",
            root_window.shape(),
            config.layout.shape()
        );
        if !config.layout.shape().fits_in(&root_window.shape()) {
            bail!("Layout doesn't fit in root window : {:?}", config.layout);
        }
        println!("DBG: Checked layout config");
        // Check modules
        for module in config.modules.iter() {
            if !module.shape().fits_in(&config.layout.shape()) {
                bail!("Module doesn't fit in layout: {module:?}");
            }
        }
        println!("DBG: Checked modules config (modules in layout)");
        // let mut modules_iter = config.modules.iter();
        // while let Some(module) = modules_iter.next() {
        //     for other in modules_iter.clone() {
        //         if other.shape().overlaps_with(&module.shape()) {
        //             println!("WARNING: Modules overlap: {module:?}, {other:?}");
        //         }
        //     }
        // }
        // println!("DBG: Checked modules config (modules)");

        Ok(Self {
            root_window,
            config,
        })
    }
}
