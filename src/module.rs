//! This crate defines the [`Module`] enum, containing all the supported modules.
//!
use serde::Deserialize;

use crate::geometry::Rectangle;
use crate::root_window::DisplayOnRoot;

pub mod draw_circle;
pub mod hello_world;
#[cfg(feature = "freetype")]
pub mod hello_world_freetype;
pub mod list;

pub use draw_circle::DrawCircleMod;
pub use hello_world::HelloWorldMod;
#[cfg(feature = "freetype")]
pub use hello_world_freetype::HelloWorldFreetypeMod;
pub use list::ListMod;

/// The [`Module`] trait: a [`Module`] needs to be able to display its content
/// on screen.
#[derive(Deserialize, Debug)]
pub enum Module {
    HelloWorld(HelloWorldMod),
    #[cfg(feature = "freetype")]
    HelloWorldFreetype(HelloWorldFreetypeMod),
    DrawCircle(DrawCircleMod),
    List(ListMod),
}

impl Default for Module {
    fn default() -> Self {
        Module::HelloWorld(HelloWorldMod::default())
    }
}

impl Module {
    // pub fn display(&self) {
    //     match self {
    //         // TODO: handle error
    //         Self::HelloWorld(module) => module.display().unwrap(),
    //         Self::DrawCircle(module) => module.display().unwrap(),
    //     }
    // }

    pub fn shape(&self) -> Rectangle {
        match self {
            Self::HelloWorld(module) => module.shape(),
            #[cfg(feature = "freetype")]
            Self::HelloWorldFreetype(module) => module.shape(),
            Self::DrawCircle(module) => module.circle.into(),
            Self::List(module) => module.shape(),
        }
    }
}

impl DisplayOnRoot for Module {
    fn display_content(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        screen: &xcb::x::Screen,
    ) {
        match self {
            Self::HelloWorld(module) => module.display_content(gc, drawable, conn, screen),
            #[cfg(feature = "freetype")]
            Self::HelloWorldFreetype(module) => module.display_content(gc, drawable, conn, screen),
            Self::DrawCircle(module) => module.display_content(gc, drawable, conn, screen),
            Self::List(module) => module.display_content(gc, drawable, conn, screen),
        }
    }

    // fn create_gc(
    //     &self,
    //     gc: xcb::x::Gcontext,
    //     drawable: xcb::x::Drawable,
    //     conn: &xcb::Connection,
    //     screen: &xcb::x::Screen,
    // ) {
    //     match self {
    //         Self::HelloWorld(module) => module.create_gc(gc, drawable, conn, screen),
    //         Self::DrawCircle(module) => module.create_gc(gc, drawable, conn, screen),
    //     }
    // }
}
