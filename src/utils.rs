//! This module contains various util functions
use crate::geometry::Area;
#[cfg(feature = "freetype")]
use freetype::GlyphSlot;
use xcb::x::Char2b;

/// Returns a 2-byte string proper to be used by xcb.
///
/// Panics if the string contains chars represented with more than 2 bytes
pub fn create_char2b_vec(s: &str) -> Vec<Char2b> {
    let mut char2b_vec = Vec::new();
    for c in s.chars() {
        let mut b = [0; 1];
        c.encode_utf16(&mut b);
        char2b_vec.push(xcb::x::Char2b {
            byte1: ((b[0] & 0xFF00) >> 8) as u8,
            byte2: (b[0] & 0x00FF) as u8,
        });
    }
    char2b_vec
}

/// Compute the extents of a String given a fontname
pub fn compute_extents(fontname: &str, s: &str) -> xcb::Result<Area> {
    let (conn, screen_num) = xcb::Connection::connect(None)?;
    let setup = conn.get_setup();
    let screen = setup.roots().nth(screen_num as usize).unwrap();
    let window = screen.root();
    let drawable = xcb::x::Drawable::Window(window);

    let font: xcb::x::Font = conn.generate_id();
    conn.send_request(&xcb::x::OpenFont {
        fid: font,
        name: fontname.as_bytes(),
    });
    // Create custom GC
    let gc: xcb::x::Gcontext = conn.generate_id();
    conn.send_request(&xcb::x::CreateGc {
        cid: gc,
        drawable,
        value_list: &[
            xcb::x::Gc::Foreground(screen.black_pixel()),
            xcb::x::Gc::Background(screen.white_pixel()),
            xcb::x::Gc::Font(font),
        ],
    });
    let cookie = conn.send_request(&xcb::x::QueryTextExtents {
        font: xcb::x::Fontable::Gcontext(gc),
        string: &create_char2b_vec(s),
    });
    let reply = conn.wait_for_reply(cookie)?;
    println!(
        "DBG: {s:?} extents: overall_ascent = {}, overall_descent = {}, overall_width = {}",
        reply.overall_ascent(),
        reply.overall_descent(),
        reply.overall_width()
    );
    Ok(Area::new(
        reply.overall_ascent() + reply.overall_descent(),
        reply.overall_width() as i16,
    ))
}

/// Create points from a Freetype Glyph object.
///
/// WARNING: this function assumes the bitmap is in monochrome mode (1 bit per pixel).
#[cfg(feature = "freetype")]
pub fn gen_points_from_glyph(
    glyph: &GlyphSlot,
    x_offset: i16,
    y_offset: i16,
) -> Vec<xcb::x::Point> {
    let bitmap = glyph.bitmap();
    // println!(
    //     "rows: {}, width: {}, pitch: {}, buffer_len: {}, pixel mode: {:?}, buffer: {:?}",
    //     bitmap.rows(),
    //     bitmap.width(),
    //     bitmap.pitch(),
    //     bitmap.buffer().len(),
    //     bitmap.pixel_mode().unwrap(),
    //     bitmap.buffer()
    // );
    let mut points = Vec::new();

    let mut x = glyph.bitmap_left();
    let mut y = glyph.bitmap_top();
    for (pos, byte) in bitmap.buffer().iter().enumerate() {
        if pos != 0 && pos % bitmap.pitch() as usize == 0 {
            y -= 1;
            x = 0;
        }
        for i in 0..8 {
            if (byte << i) & 0b10000000_u8 != 0 {
                points.push(xcb::x::Point {
                    x: x as i16 + x_offset,
                    y: y_offset - y as i16,
                });
            }
            x += 1;
            if x as i32 >= bitmap.width() {
                break;
            }
        }
    }

    points
}
