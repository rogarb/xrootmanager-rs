//! This module defines the format of the config file
//!
use serde::Deserialize;

use crate::geometry::{Area, Point, Rectangle};
use crate::module::Module;
use crate::root_window::RootWindowAttributes;

/// The structure of the config file
#[derive(Deserialize)]
pub struct Config {
    pub layout: LayoutConfig,
    pub modules: Vec<Module>,
}

// TODO: add fields
// shape: ShapeEnum // Move to Layout struct ??
// center: Option<Point>
// dimensions: Option<Area>
// border: Option<usize> // Create a rectangle out of the root window adding a border
#[derive(Deserialize, Debug)]
pub struct LayoutConfig {
    origin: Option<Point>,
    dimensions: Option<Area>,
}

impl LayoutConfig {
    pub fn shape(&self) -> Rectangle {
        let attr = RootWindowAttributes::new().expect("Unable to get root window attributes");
        let upper_left_corner = self.origin.unwrap_or_default();
        let area = self.dimensions.unwrap_or_else(|| attr.into());
        Rectangle::new(upper_left_corner, area)
    }
}
