//! This module contains functions related to the root window management
//!
use crate::geometry::{Area, Point, Rectangle};

/// A trait to represent the ability to display on the root window
pub trait DisplayOnRoot {
    fn display_content(
        &self,
        gc: xcb::x::Gcontext,
        drawable: xcb::x::Drawable,
        conn: &xcb::Connection,
        screen: &xcb::x::Screen,
    );

    // fn create_gc(
    //     &self,
    //     gc: xcb::x::Gcontext,
    //     drawable: xcb::x::Drawable,
    //     conn: &xcb::Connection,
    //     screen: &xcb::x::Screen,
    // );

    // /// Display custom content on root using a custom graphical context
    // fn display(&self) -> xcb::Result<()> {
    //     let (conn, screen_num) = xcb::Connection::connect(None).unwrap();
    //     let setup = conn.get_setup();
    //     let screen = setup.roots().nth(screen_num as usize).unwrap();
    //     let window = screen.root();
    //     conn.send_request(&xcb::x::ChangeWindowAttributes {
    //         window,
    //         value_list: &[
    //             xcb::x::Cw::BackPixel(screen.black_pixel()),
    //             xcb::x::Cw::EventMask(xcb::x::EventMask::EXPOSURE),
    //         ],
    //     });

    //     // Create custom GC
    //     let drawable = xcb::x::Drawable::Window(window);
    //     let gc: xcb::x::Gcontext = conn.generate_id();
    //     self.create_gc(gc, drawable, &conn, screen);

    //     conn.flush()?;
    //     loop {
    //         let event = match conn.wait_for_event() {
    //             Err(xcb::Error::Connection(xcb::ConnError::Connection)) => {
    //                 // graceful shutdown, likely "x" close button clicked in title bar
    //                 break;
    //             }
    //             Err(err) => {
    //                 panic!("unexpected error: {:#?}", err);
    //             }
    //             Ok(event) => event,
    //         };
    //         if let xcb::Event::X(xcb::x::Event::Expose(_ev)) = event {
    //             self.display_content(gc, drawable, &conn);

    //             /* We flush the request */
    //             conn.flush()?;
    //         }
    //     }
    //     Ok(())
    // }
}

/// A struct to store various attributes of the root window
#[derive(Default)]
pub struct RootWindowAttributes {
    pub height: i16,
    pub width: i16,
}

impl RootWindowAttributes {
    pub fn new() -> xcb::Result<Self> {
        // Connect to the X server
        let (conn, screen_num) = xcb::Connection::connect(None)?;
        let setup = conn.get_setup();

        let screen = setup.roots().nth(screen_num as usize).unwrap();
        let window = screen.root();
        let cookie = conn.send_request(&xcb::x::GetGeometry {
            drawable: xcb::x::Drawable::Window(window),
        });
        let reply = conn.wait_for_reply(cookie)?;
        let (height, width) = (reply.height() as i16, reply.width() as i16);
        Ok(Self { height, width })
    }

    pub fn shape(&self) -> Rectangle {
        Rectangle::new(Point::default(), Area::new(self.height, self.width))
    }
}
