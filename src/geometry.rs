//! This module contains different structs holding geometrical data.
//!
use crate::root_window::RootWindowAttributes;
use serde::Deserialize;

pub mod circle;
pub mod square;

/// The [`Area`] struct holds theght and the width of a geometrical object.
#[derive(Default, Clone, Deserialize, Copy, Debug)]
pub struct Area {
    pub height: i16,
    pub width: i16,
}

impl Area {
    pub fn new(height: i16, width: i16) -> Self {
        Self { height, width }
    }
}

impl From<RootWindowAttributes> for Area {
    fn from(attr: RootWindowAttributes) -> Self {
        Self::new(attr.height, attr.width)
    }
}

/// The [`Point`] struct holds the cartesian position (x, y) of a geometrical
/// object.
#[derive(Default, Debug, Clone, PartialEq, Copy, Deserialize)]
pub struct Point {
    pub x: i16,
    pub y: i16,
}

impl Point {
    pub fn new(x: i16, y: i16) -> Self {
        Self { x, y }
    }
}

impl Point {
    pub fn as_x_point(&self) -> xcb::x::Point {
        xcb::x::Point {
            x: self.x,
            y: self.y,
        }
    }
}

/// The [`Rectangle`] struct defines a rectangle shaped [`Shape`].
#[derive(Default, Clone, Debug, Deserialize)]
pub struct Rectangle {
    pub upper_left_corner: Point,
    pub dimensions: Area,
    //parent: Option<&'a dyn Shape>,
}

impl IntoIterator for Rectangle {
    type Item = Point;
    type IntoIter = RectanglePoints;
    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            rectangle: self,
            current: None,
        }
    }
}

impl Rectangle {
    pub fn new(upper_left_corner: Point, dimensions: Area) -> Self {
        assert!(dimensions.width > 0, "A Rectangle cannot have a width of 0");
        assert!(
            dimensions.height > 0,
            "A Rectangle cannot have a height of 0"
        );
        Self {
            upper_left_corner,
            dimensions,
        }
    }

    pub fn points(&self) -> impl Iterator {
        self.clone().into_iter()
    }

    /// Check if two [`Shape`]s overlap, i.e. have at least one point in common
    pub fn overlaps_with(&self, other: &Self) -> bool {
        self.clone()
            .into_iter()
            .any(|p| other.clone().into_iter().any(|q| p == q))
    }

    /// Check if a [`Shape`] fits in another, i.e. all clone().into_iter of self exist in
    /// other.
    pub fn fits_in(&self, other: &Self) -> bool {
        self.upper_left_corner.x >= other.upper_left_corner.x
            && self.upper_left_corner.y >= other.upper_left_corner.y
            && self.dimensions.width <= other.dimensions.width
            && self.dimensions.height <= other.dimensions.height
    }

    /// Create an X rectangle out of self.
    pub fn as_x_rectangle(&self) -> xcb::x::Rectangle {
        xcb::x::Rectangle {
            x: self.upper_left_corner.x as _,
            y: self.upper_left_corner.y as _,
            width: self.dimensions.width as _,
            height: self.dimensions.height as _,
        }
    }
}

impl From<square::Square> for Rectangle {
    fn from(square: square::Square) -> Self {
        Self::new(
            square.upper_left_corner,
            Area::new(square.side_length as i16, square.side_length as i16),
        )
    }
}

impl From<circle::Circle> for Rectangle {
    fn from(circle: circle::Circle) -> Self {
        Self::new(
            circle.outer_square().upper_left_corner,
            Area::new(
                circle.outer_square().side_length as i16,
                circle.outer_square().side_length as i16,
            ),
        )
    }
}

/// A struct to iterate over the [`Point`]s of a [`Rectangle`]
pub struct RectanglePoints {
    rectangle: Rectangle,
    current: Option<Point>,
}

impl std::iter::Iterator for RectanglePoints {
    type Item = Point;
    fn next(&mut self) -> Option<Self::Item> {
        let retval = match self.current {
            None => Some(self.rectangle.upper_left_corner),
            Some(point) => {
                if point.x
                    >= self.rectangle.upper_left_corner.x + self.rectangle.dimensions.width - 1
                {
                    if point.y
                        >= self.rectangle.upper_left_corner.y + self.rectangle.dimensions.height - 1
                    {
                        None
                    } else {
                        Some(Point::new(self.rectangle.upper_left_corner.x, point.y + 1))
                    }
                } else {
                    Some(Point::new(point.x + 1, point.y))
                }
            }
        };
        self.current = retval;
        retval
    }
}

#[cfg(test)]
mod tests {
    use crate::geometry::*;

    #[test]
    #[should_panic]
    fn empty_rectangle() {
        let _empty_rectangle = Rectangle::new(Point::default(), Area::new(0, 0));
    }

    #[test]
    fn into_iter() {
        let rectangle = Rectangle::new(Point::new(0, 0), Area::new(2, 2));
        let mut iter = rectangle.into_iter();
        assert_eq!(iter.next(), Some(Point::new(0, 0)));
        assert_eq!(iter.next(), Some(Point::new(1, 0)));
        assert_eq!(iter.next(), Some(Point::new(0, 1)));
        assert_eq!(iter.next(), Some(Point::new(1, 1)));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn fits_in() {
        let small_rectangle = Rectangle::new(Point::new(1, 1), Area::new(2, 2));
        let big_rectangle = Rectangle {
            upper_left_corner: Point::new(0, 0),
            dimensions: Area::new(5, 5),
        };
        assert!(small_rectangle.fits_in(&big_rectangle));
    }

    #[test]
    fn overlaps_with() {
        let small_rectangle = Rectangle::new(Point::new(1, 1), Area::new(2, 2));
        let big_rectangle = Rectangle::new(Point::new(0, 0), Area::new(5, 5));
        assert!(small_rectangle.overlaps_with(&big_rectangle));
    }

    #[test]
    fn no_overlap() {
        let left_rectangle = Rectangle::new(Point::new(0, 0), Area::new(4, 4));
        let right_rectangle = Rectangle::new(Point::new(6, 0), Area::new(4, 4));
        assert!(!left_rectangle.overlaps_with(&right_rectangle));
    }

    #[test]
    fn fit_big_rectangles() {
        let big_rectangle = Rectangle::new(Point::new(0, 0), Area::new(800, 600));
        let bigger_rectangle = Rectangle::new(Point::new(0, 0), Area::new(1024, 768));
        assert!(big_rectangle.fits_in(&bigger_rectangle));
    }

    #[test]
    fn fit_in_self() {
        let rectangle = Rectangle::new(Point::new(0, 0), Area::new(80, 60));
        assert!(rectangle.fits_in(&rectangle));
    }
}
