//! This module contains the implementation of the [`Square`] [`Shape`].
//!
use serde::Deserialize;

use crate::geometry::Point;

/// The [`Square`] struct defines a rectangle shaped [`Shape`].
#[derive(Default, Clone, Deserialize)]
pub struct Square {
    pub upper_left_corner: Point,
    pub side_length: usize,
}

impl IntoIterator for Square {
    type Item = Point;
    type IntoIter = SquarePoints;
    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            square: self,
            current: None,
        }
    }
}

impl Square {
    pub fn new(upper_left_corner: Point, side_length: usize) -> Self {
        assert!(side_length > 0, "A Square cannot have a side length of 0");
        Self {
            upper_left_corner,
            side_length,
        }
    }

    pub fn from_center(center: Point, side_length: usize) -> Self {
        assert!(
            side_length % 2 == 1,
            "The side length must be even to define a square from its center"
        );
        let half_side = (side_length - 1) / 2;
        assert!(
            center.x >= half_side as i16 && center.y >= half_side as i16,
            "The square is out of screen bonds"
        );
        let upper_left_corner =
            Point::new(center.x - half_side as i16, center.y - half_side as i16);
        Self::new(upper_left_corner, side_length)
    }

    pub fn points(&self) -> impl Iterator {
        self.clone().into_iter()
    }
}

/// A struct to iterate over the [`Point`]s of a [`Square`]
pub struct SquarePoints {
    square: Square,
    current: Option<Point>,
}

impl std::iter::Iterator for SquarePoints {
    type Item = Point;
    fn next(&mut self) -> Option<Self::Item> {
        let retval = match self.current {
            None => Some(self.square.upper_left_corner),
            Some(point) => {
                if point.x >= self.square.upper_left_corner.x + self.square.side_length as i16 - 1 {
                    if point.y
                        >= self.square.upper_left_corner.y + self.square.side_length as i16 - 1
                    {
                        None
                    } else {
                        Some(Point::new(self.square.upper_left_corner.x, point.y + 1))
                    }
                } else {
                    Some(Point::new(point.x + 1, point.y))
                }
            }
        };
        self.current = retval;
        retval
    }
}

#[cfg(test)]
mod tests {
    use crate::geometry::square::Square;
    use crate::geometry::Point;

    #[test]
    #[should_panic]
    fn empty_square() {
        let _empty_square = Square::new(Point::default(), 0);
    }

    // #[test]
    // fn contiguous_squares() {
    //     let first = Square::new(Point::new(0, 0), 5);
    //     let second = Square::new(Point::new(5, 0), 5);
    //     assert!(!first.fits_in(&second));
    //     assert!(!first.overlaps_with(&second));
    // }

    #[test]
    fn into_iter() {
        let square = Square::new(Point::new(0, 0), 2);
        let mut iter = square.into_iter();
        assert_eq!(iter.next(), Some(Point::new(0, 0)));
        assert_eq!(iter.next(), Some(Point::new(1, 0)));
        assert_eq!(iter.next(), Some(Point::new(0, 1)));
        assert_eq!(iter.next(), Some(Point::new(1, 1)));
        assert_eq!(iter.next(), None);
    }

    // #[test]
    // fn fits_in() {
    //     let small_square = Square::new(Point::new(1, 1), 2);
    //     let big_square = Square::new(Point::new(0, 0), 5);
    //     assert!(small_square.fits_in(&big_square));
    // }

    // #[test]
    // fn overlaps_with() {
    //     let small_square = Square::new(Point::new(1, 1), 2);
    //     let big_square = Square::new(Point::new(0, 0), 5);
    //     assert!(small_square.overlaps_with(&big_square));
    // }

    // #[test]
    // fn no_overlap() {
    //     let left_square = Square::new(Point::new(0, 0), 4);
    //     let right_square = Square::new(Point::new(6, 0), 4);
    //     assert!(!left_square.overlaps_with(&right_square));
    // }
}
