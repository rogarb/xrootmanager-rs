//! This module contains the implementation of the [`Circle`] [`Shape`].
//!
use serde::Deserialize;

use crate::geometry::square::{Square, SquarePoints};
use crate::geometry::Point;

/// The [`Circle`] struct defines a rectangle shaped [`Shape`].
#[derive(Default, Clone, Deserialize, Debug, Copy)]
pub struct Circle {
    center: Point,
    radius: usize,
}

impl IntoIterator for Circle {
    type Item = Point;
    type IntoIter = CirclePoints;
    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            square_iter: self.outer_square().into_iter(),
            circle: self,
            // current: None,
        }
    }
}

impl Circle {
    pub fn new(center: Point, radius: usize) -> Self {
        assert!(radius > 0, "A Circle cannot have a radius of 0");
        Self { center, radius }
    }

    pub fn outer_square(&self) -> Square {
        Square::from_center(self.center, self.radius * 2 + 1)
    }

    pub fn contains(&self, point: Point) -> bool {
        ((self.center.x as f32 - point.x as f32).powi(2)
            + (self.center.y as f32 - point.y as f32).powi(2))
        .sqrt()
            <= (self.radius as f32)
    }

    pub fn points(&self) -> impl Iterator {
        self.clone().into_iter()
    }
}

/// A struct to iterate over the [`Point`]s of a [`Circle`]
pub struct CirclePoints {
    circle: Circle,
    square_iter: SquarePoints,
    //current: Option<Point>,
}

impl std::iter::Iterator for CirclePoints {
    type Item = Point;
    fn next(&mut self) -> Option<Self::Item> {
        // This loop shouldn't be written as a for loop as self.square_iter is
        // mutated on each call of next()
        while let Some(point) = self.square_iter.next() {
            if self.circle.contains(point) {
                return Some(point);
            }
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use crate::geometry::circle::Circle;
    use crate::geometry::Point;

    #[test]
    #[should_panic]
    fn empty_circle() {
        let _empty_circle = Circle::new(Point::default(), 0);
    }

    #[test]
    fn into_iter() {
        let circle = Circle::new(Point::new(2, 2), 1);
        let mut iter = circle.into_iter();
        assert_eq!(iter.next(), Some(Point::new(2, 1)));
        assert_eq!(iter.next(), Some(Point::new(1, 2)));
        assert_eq!(iter.next(), Some(Point::new(2, 2)));
        assert_eq!(iter.next(), Some(Point::new(3, 2)));
        assert_eq!(iter.next(), Some(Point::new(2, 3)));
        assert_eq!(iter.next(), None);
    }

    // #[test]
    // fn fits_in() {
    //     let small_circle = Circle::new(Point::new(10, 10), 2);
    //     let big_circle = Circle::new(Point::new(10, 10), 5);
    //     assert!(small_circle.fits_in(&big_circle));
    // }

    // #[test]
    // fn overlaps_with() {
    //     let small_circle = Circle::new(Point::new(12, 10), 5);
    //     let big_circle = Circle::new(Point::new(10, 10), 5);
    //     assert!(small_circle.overlaps_with(&big_circle));
    // }

    // #[test]
    // fn no_overlap() {
    //     let left_circle = Circle::new(Point::new(10, 10), 4);
    //     let right_circle = Circle::new(Point::new(20, 10), 4);
    //     println!(
    //         "{:?}\n{:?}",
    //         left_circle.clone().into_iter().collect::<Vec<Point>>(),
    //         right_circle.clone().into_iter().collect::<Vec<Point>>()
    //     );
    //     assert!(!left_circle.overlaps_with(&right_circle));
    // }
}
