//! This module contains the implementation of the [`Rectangle`] [`Shape`].
//!
use serde::Deserialize;

use crate::geometry::{Area, Point, Shape, Square};
