use anyhow::anyhow;
use clap::Parser;

use std::env;
use std::ffi::OsStr;
use std::path::PathBuf;

mod config;
mod geometry;
mod layout;
mod module;
mod root_window;
mod utils;

use crate::layout::Layout;

/// Display something on the root window.
#[derive(Parser)]
struct Cli {
    /// Test mode, prints "Hello world" on the upper left corner.
    #[clap(short, long)]
    test: bool,
    /// Use the specified config file (see below for default paths).
    #[clap(short, long = "config", value_parser = path_validator)]
    config_file: Option<PathBuf>,
    /// Don't go into the background. The errors/warnings are printed on
    /// standard output.
    #[clap(short, long)]
    no_daemon: bool,
}

/// Checks that the given path exists
fn path_validator(path: &str) -> Result<PathBuf, String> {
    let path = PathBuf::from(path);
    if !path.exists() {
        Err(format!("{}: path does not exist", path.display()))
    } else {
        Ok(path)
    }
}

/// Returns a config file looking at different default places
fn default_config() -> anyhow::Result<PathBuf> {
    let mut default_places = Vec::new();

    // Build default places from env simply logging env errors on stdout
    match env::var("HOME") {
        Ok(val) => default_places.push(PathBuf::from(format!("{}/.xrootmanager", val))),
        Err(e) => println!("{e}"),
    }
    match env::var("XDG_CONFIG_HOME") {
        Ok(val) => default_places.push(PathBuf::from(format!("{}/xrootmanager", val))),
        Err(e) => println!("XDG_CONFIG_HOME: {e}"),
    }
    match env::var("XDG_DATA_HOME") {
        Ok(val) => default_places.push(PathBuf::from(format!("{}/xrootmanager/config", val))),
        Err(e) => println!("XDG_DATA_HOME: {e}"),
    }

    // Return the first existing file
    for file in default_places {
        if file.exists() {
            return Ok(file);
        }
    }

    // No file has been found, return an error
    Err(anyhow!(
        "No configuration file found. Use --config or create a config file at a default location",
    ))
}

fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();
    if !cli.no_daemon {
        let stdout = std::fs::File::create("/tmp/daemon.out").unwrap();
        let stderr = std::fs::File::create("/tmp/daemon.err").unwrap();

        let daemonize = daemonize::Daemonize::new()
            .stdout(stdout) // Redirect stdout to `/tmp/daemon.out`.
            .stderr(stderr); // Redirect stderr to `/tmp/daemon.err`.

        daemonize.start()?;
    }

    let config_file = match cli.config_file {
        Some(file) => file,
        None => default_config()?,
    };

    let layout = Layout::new(config_file.as_ref())?;
    layout.display()?;
    Ok(())
}
