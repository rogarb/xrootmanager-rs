xrootmanager-rs
===============

A root window content manager for X11 written in Rust.

Features
--------

 * Manage the content displayed on the root window
 * Display text (notes, todolists...) or graphics (cheatsheets, images...)
 * Can be daemonized (by default) or not.
 * When daemonized, output is logged using system logging
 * When not daemonized, output is sent to stderr

Installation
------------

```$ cargo install --git https://gitlab.com/rogarb/xrootmanager-rs```

Usage
-----

This program is designed to run as a daemon, for example started at the 
beginning of the Xorg user session.

Design
------

The root window is configured using a Layout object.
```
Layout {
    max_width
    max_height
    Vec<Module> // ordered list so 
}
```

Create from config file (toml format)
Ensure no overlap of modules

Module should be a trait with a way to retrieve position and content, in order
to ensure the non overlapping of the different modules:
```
trait Module {
    fn position(&self) -> Position {} 

    fn content(&self) -> Area {}

    fn display(&self) -> Result<()> {}
}
```

Execution flow
--------------
When the program starts, it reads the config file (returning with an error if 
the configfile is not present at its default location or at the commandline 
provided path) and creates a Layout out of it.

On Layout creation, Modules are created using the settings provided by the 
config file. This step ensures that all the settings are correct and compatible
between each other.

The program then sets up the root window attributes necessary to the event loop
(mainly Expose flag) and then enters the event loop, which basically displays 
the Layout when the root window is exposed.
